aiogram==2.12.1
grpcio==1.36.1
grpcio-tools==1.36.1
sentry-sdk==1.0.0
tortoise-orm[asyncpg]==0.17.2