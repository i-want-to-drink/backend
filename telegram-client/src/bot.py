import asyncio
import grpc
import logging

from aiogram import Bot, Dispatcher, types
from aiogram.utils.markdown import text, link, escape_md
from aiogram.contrib.middlewares.logging import LoggingMiddleware

import config
import constants
import data_manager_pb2, data_manager_pb2_grpc
import middleware

from models import User, Referral
from utils import batcher, aenumerate


# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Initialize bot and dispatcher
bot = Bot(token=config.TELEGRAM_BOT_TOKEN)
dp = Dispatcher(bot)
dp.middleware.setup(LoggingMiddleware())
dp.middleware.setup(middleware.AnalyticsMiddleware())


@dp.message_handler(commands=["start"])
async def handle_start(message: types.Message):
    referral = await Referral.filter(text=message.get_args()).first()
    await User.get_or_create(
        telegram_id=message.chat.id,
        defaults={
            "telegram_username": message.chat.username,
            "referral": referral,
        },
    )

    await message.answer(constants.START_MESSAGE)


@dp.message_handler(commands=["sources"])
async def handle_sources(message: types.Message):
    await message.answer(constants.SOURCES_MESSAGE)


async def get_results(query, offset=0, limit=0):
    query = data_manager_pb2.SearchQuery(
        query=query, offset=offset, limit=limit
    )

    # TODO: move to function
    async with grpc.aio.insecure_channel(
        config.DATA_MANAGER_ADDRESS
    ) as channel:
        stub = data_manager_pb2_grpc.DataManagerStub(channel)

        try:
            async for result in stub.Search(query):
                yield result
        except grpc.RpcError as e:
            logger.error(str(e))
            raise RuntimeError


async def get_results_count(query):
    query = data_manager_pb2.CountQuery(query=query)

    async with grpc.aio.insecure_channel(
        config.DATA_MANAGER_ADDRESS
    ) as channel:
        try:
            stub = data_manager_pb2_grpc.DataManagerStub(channel)
            count = await stub.Count(query)
            return count.count
        except grpc.RpcError as e:
            logger.error(str(e))
            raise RuntimeError


@dp.message_handler()
async def query(
    message: types.Message, offset=0, limit=config.NUM_PRODUCTS_IN_MESSAGE
):
    # TODO: move business logic to function
    try:
        results = get_results(message.text, offset=offset, limit=limit)
        results_count = await get_results_count(message.text)
    except RuntimeError:
        await message.answer(constants.SERVICE_ERROR_MESSAGE)
        return

    if results_count == 0:
        await message.answer(constants.NOT_FOUND_MESSAGE)
        return

    if results_count > offset + limit:
        new_offset = offset + limit
        show_more_btn = types.InlineKeyboardButton(
            f"Показать еще {min(results_count - offset - limit, limit)} (всего {results_count})",
            callback_data=f"q:{new_offset}:{limit}#{message.text}",
        )
        reply_markup = types.InlineKeyboardMarkup().add(show_more_btn)
    else:
        reply_markup = None

    answer = []
    async for i, r in aenumerate(results):
        price = f"{round(r.product_price, 2)} ₽"
        answer.append(
            text(
                escape_md(f"{offset + i + 1}.", r.product_title, "/"),
                link(text(price, "в", r.product_source), r.product_link)
                if r.product_link
                else escape_md(price, "в", r.product_source),
            )
        )

    await message.answer(
        text(*answer, sep="\n"),
        reply_markup=reply_markup,
        parse_mode=types.ParseMode.MARKDOWN_V2,
        disable_web_page_preview=True,
    )


@dp.callback_query_handler(lambda q: q.data.startswith("q"))
async def process_query(callback_query: types.CallbackQuery):
    await callback_query.message.edit_reply_markup(reply_markup=None)
    callback_data = callback_query.data.lstrip("q:")
    message = callback_query.message
    args, message.text = callback_data.split("#")
    offset, limit = list(map(int, args.split(":")))
    await query(message, offset, limit)


current_bot_commands = {
    "sources": "Откуда сейчас собираем информацию?",
}


async def set_commands():
    commands = [
        types.BotCommand(command=cmd, description=desc)
        for cmd, desc in current_bot_commands.items()
    ]
    await bot.set_my_commands(commands)


async def main():
    await config.setup()
    await set_commands()
    await dp.start_polling()


if __name__ == "__main__":
    asyncio.run(main())