from aiogram import types, exceptions
from aiogram.dispatcher.middlewares import BaseMiddleware

from models import User, Message


class AnalyticsMiddleware(BaseMiddleware):
    """
    Middleware для обработки пользователских действий
    """

    async def on_pre_process_error(
        self, update: types.Update, error, data: dict
    ):
        if isinstance(error, exceptions.BotBlocked):
            await User.filter(telegram_id=update.message.chat.id).update(
                blocked_the_bot=True
            )

    async def on_post_process_message(
        self, message: types.Message, results, data: dict
    ):
        user, _ = await User.get_or_create(
            telegram_id=message.chat.id,
            defaults={"telegram_username": message.chat.username},
        )
        await Message.create(user=user, message=message.text)
