import os
import sentry_sdk

from tortoise import Tortoise

dbname = os.getenv("POSTGRES_DB")
dbuser = os.getenv("POSTGRES_USER")
dbpassword = os.getenv("POSTGRES_PASSWORD")
dbhost = os.getenv("POSTGRES_HOST")

DB_DSN = f"dbname={dbname} user={dbuser} password={dbpassword} host={dbhost}"
DB_URI = f"postgres://{dbuser}:{dbpassword}@{dbhost}:5432/{dbname}"

NUM_PRODUCTS_IN_MESSAGE = 10

TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")

# services
DATA_MANAGER_ADDRESS = os.getenv("DATA_MANAGER_ADDRESS")


async def setup():
    await Tortoise.init(db_url=DB_URI, modules={"models": ["models"]})
    await Tortoise.generate_schemas()
