import uuid

from tortoise.models import Model
from tortoise import fields


class TimestampMixin:
    created_at = fields.DatetimeField(null=True, auto_now_add=True)
    modified_at = fields.DatetimeField(null=True, auto_now=True)


class AbstractBaseMode(Model):
    id = fields.UUIDField(pk=True)

    class Meta:
        abstract = True


class User(TimestampMixin, AbstractBaseMode):
    telegram_id = fields.IntField(index=True, unique=True)
    telegram_username = fields.CharField(max_length=30, null=True)
    blocked_the_bot = fields.BooleanField(default=False)
    referral = fields.ForeignKeyField("models.Referral", null=True)


class Message(TimestampMixin, AbstractBaseMode):
    user = fields.ForeignKeyField("models.User")
    message = fields.TextField()


class Referral(TimestampMixin, AbstractBaseMode):
    @staticmethod
    def _generate_short_random_string(length=8):
        return uuid.uuid4().hex[:length]

    text = fields.CharField(
        40,
        default=_generate_short_random_string,
        unique=True,
        index=True,
    )
    reason = fields.TextField(help="Для какой кампании была сделана ссылка")
