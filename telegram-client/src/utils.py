from itertools import islice


async def batcher(iterable, batch_size):
    batch = []
    async for elem in iterable:
        batch.append(elem)
        if len(batch) >= batch_size:
            yield batch
            batch = []


async def aenumerate(asequence, start=0):
    """Asynchronously enumerate an async iterator from a given start value"""
    n = start
    async for elem in asequence:
        yield n, elem
        n += 1
