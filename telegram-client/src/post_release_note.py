import sys
import asyncio

from aiogram import Bot, types
from aiogram.utils.markdown import escape_md

import config

from models import User


async def main():
    bot = Bot(token=config.TELEGRAM_BOT_TOKEN)

    await config.setup()

    _, release_note_path = sys.argv
    with open(release_note_path) as f:
        release_note_text = f.read()

    async for (telegram_id, telegram_username) in User.filter(
        blocked_the_bot=False
    ).values_list("telegram_id", "telegram_username"):
        print(f"Sending release notes to {telegram_username}")
        await bot.send_message(
            telegram_id,
            release_note_text,
            parse_mode=types.ParseMode.HTML,
        )
        await asyncio.sleep(0.3)


if __name__ == "__main__":
    asyncio.run(main())