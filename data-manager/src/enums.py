from enum import Enum


class EStore(Enum):
    # Done
    AUCHAN = "Ашан"
    LENTA = "Лента"
    PEREKRESTOK = "Перекресток"
    AV = "Азбука вкуса"

    # In progress
    KRASNOEIBELOE = "Красное & Белое"

    # Planned
    TVOYDOM = "Твой Дом"
    METRO = "METRO Cash and Carry"
    OKEY = "О'КЕЙ"
    GLOBUS = "Глобус"
    WINESTYLE = "Wine Style"
    AMWINE = "Ароматный мир"

    # https://edadeal.ru/moskva/retailers?type=alkogolnye-magaziny
    # https://winestreet.ru/champagne-and-sparkling-wines/?sort=price-desc

    def __str__(self):
        return self.name