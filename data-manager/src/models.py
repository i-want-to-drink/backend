from tortoise.models import Model
from tortoise import fields

from enums import EStore


class TimestampMixin:
    created_at = fields.DatetimeField(null=True, auto_now_add=True)
    modified_at = fields.DatetimeField(null=True, auto_now=True)


class AbstractBaseMode(Model):
    id = fields.UUIDField(pk=True)

    class Meta:
        abstract = True


class Brand(TimestampMixin, AbstractBaseMode):
    title = fields.CharField(255, unique=True)

    def __str__(self):
        return self.title


class Category(TimestampMixin, AbstractBaseMode):
    title = fields.CharField(255, unique=True)

    def __str__(self):
        return self.title


class Product(TimestampMixin, AbstractBaseMode):
    bk = fields.TextField()
    title = fields.TextField()
    brand = fields.ForeignKeyField("models.Brand", null=True)
    category = fields.ForeignKeyField("models.Category", null=True)
    source = fields.CharEnumField(EStore)

    url = fields.TextField()
    image = fields.TextField()
    abv = fields.FloatField(null=True)
    volume = fields.FloatField(null=True)


class Price(TimestampMixin, AbstractBaseMode):
    product = fields.ForeignKeyField("models.Product", related_name="prices")
    in_stock = fields.BooleanField()
    value = fields.FloatField()
