import os
import sentry_sdk

from tortoise import Tortoise

dbname = os.getenv("POSTGRES_DB")
dbuser = os.getenv("POSTGRES_USER")
dbpassword = os.getenv("POSTGRES_PASSWORD")
dbhost = "dm_db"

DB_DSN = f"dbname={dbname} user={dbuser} password={dbpassword} host={dbhost}"
DB_URI = f"postgres://{dbuser}:{dbpassword}@{dbhost}:5432/{dbname}"

INIT_SENTRY = os.getenv("INIT_SENTRY")


async def setup():
    await Tortoise.init(db_url=DB_URI, modules={"models": ["models"]})
    await Tortoise.generate_schemas()

    if INIT_SENTRY:
        sentry_sdk.init(
            "https://b3e2ede4ee084559b9e6b1b23d6f449d@o560031.ingest.sentry.io/5695326",
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            # We recommend adjusting this value in production.
            traces_sample_rate=1.0,
        )
