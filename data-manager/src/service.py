import asyncio
import grpc
import logging

from typing import AsyncIterable, Iterable

import core
from config import setup
from models import Product, Price
import data_manager_pb2, data_manager_pb2_grpc


class DataManagerServicer(data_manager_pb2_grpc.DataManagerServicer):
    async def Search(self, request, context):
        async for product_id in core.search(
            request.query, request.offset, request.limit
        ):
            product = await Product.get(id=product_id)
            product_price = (
                await Price.filter(product_id=product_id)
                .order_by("-modified_at")
                .first()
            )

            yield data_manager_pb2.SearchResult(
                product_title=product.title,
                product_link=product.url,
                product_price=product_price.value,
                product_source=product.source.value,
            )

    async def Count(self, request, context):
        count = await core.count(request.query)
        return data_manager_pb2.CountResult(count=count)


async def serve() -> None:
    await setup()

    server = grpc.aio.server()
    data_manager_pb2_grpc.add_DataManagerServicer_to_server(
        DataManagerServicer(), server
    )
    server.add_insecure_port("[::]:50051")
    await server.start()
    await server.wait_for_termination()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.get_event_loop().run_until_complete(serve())