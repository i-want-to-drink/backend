import asyncio

from scrapers.alco_scraper import AlcoScraper
from scrapers.utils import ProductSummary, ProductParseException
from enums import EStore


class LentaAlcoScraper(AlcoScraper):
    BASE_URL = "https://lenta.com"
    START_PATH = "/catalog/alkogolnye-napitki/"

    PRODUCT_CARD_SELECTOR = "a.sku-card-small"
    NEXT_PAGE_LINK_SELECTOR = "ul.pagination > li.next > a"

    SOURCE = EStore.LENTA

    async def get_next_page_url(self, soup):
        # https://developers.google.com/search/blog/2011/09/pagination-with-relnext-and-relprev
        # <a rel="32 == 32 ? 'nofollow' : 'next' />
        a = soup.select_one(self.NEXT_PAGE_LINK_SELECTOR)
        rel = a["rel"]
        if rel[0] == rel[2]:
            return
        return a["href"]

    def get_product_summary(self, product_card) -> ProductSummary:
        product_image = product_card.select_one("img")["src"]
        product_title = product_card.select_one(
            ".sku-card-small__title"
        ).text.strip()

        product_path = product_card["href"]
        product_url = self.BASE_URL + product_path
        product_in_stock = product_card.find(".basket-sku-control") is not None

        try:
            price_int_part = product_card.select_one(
                ".sku-price--primary > .sku-price__integer"
            ).text
            price_float_part = product_card.select_one(
                ".sku-price--primary > .sku-price__fraction"
            ).text
            product_price = (
                int("".join(filter(str.isdigit, price_int_part)))
                + int("".join(filter(str.isdigit, price_float_part))) * 0.01
            )
        except Exception as e:
            raise ProductParseException("Error in {product_url}: {e}")

        product_summary = ProductSummary(
            title=product_title,
            image=product_image,
            path=product_path,
            url=product_url,
            price=product_price,
            in_stock=product_in_stock,
            source=self.SOURCE,
        )

        return product_summary

    @staticmethod
    def get_abv_from_product_info(soup):
        try:
            return soup.find("label", string="Крепость (%)").parent.dd.text
        except Exception:
            pass

    @staticmethod
    def get_bk_from_product_info(soup):
        return soup.select_one(".sku-page__code-info").text.strip()

    @staticmethod
    def get_category_from_product_info(soup):
        return soup.select("a.breadcrumbs__crumb")[-2].text.strip()

    @staticmethod
    def get_volume_from_product_info(soup):
        try:
            volume = soup.find("label", string="Литраж").parent.parent.dd.text
            return float(volume.rstrip(" L")) * 1_000
        except Exception:
            pass

    @staticmethod
    def get_brand_from_product_info(soup):
        try:
            return soup.find(
                "label", string="Бренд"
            ).parent.parent.a.text.strip()
        except Exception as e:
            pass
