import aiohttp
import logging

from bs4 import BeautifulSoup
from dataclasses import asdict
from typing import AsyncIterator

from models import Brand, Category, Product, Price
from enums import EStore
from scrapers.utils import ProductSummary, ProductInfo, ProductParseException


class AlcoScraper:
    """
    USAGE:
    with AlcoScraper() as as:
        as.do_scraping()
    """

    BASE_URL: str
    START_PATH: str

    PRODUCT_CARD_SELECTOR: str
    NEXT_PAGE_LINK_SELECTOR: str

    SOURCE: EStore

    _session: aiohttp.ClientSession

    async def __aenter__(self):
        self._session = aiohttp.ClientSession()
        self.stats = {"category": [0, 0], "brand": [0, 0], "product": [0, 0]}
        self.logger = logging.getLogger(str(self.SOURCE))
        return self

    async def __aexit__(self, *err):
        await self._session.close()
        self._session = None
        print(self.SOURCE, self.stats)

    async def do_scraping(self):
        products = self.get_products()
        async for product in products:
            await self.save_product(product)

    async def get_products(
        self,
    ) -> AsyncIterator[ProductInfo]:
        start_url = self.BASE_URL + self.START_PATH
        products = self.get_products_from_page(start_url)
        async for product in products:
            try:
                product_info = await self.get_product_info(product)
            except ProductParseException:
                continue
            yield product_info

    async def get_products_from_page(
        self, url
    ) -> AsyncIterator[ProductSummary]:
        self.logger.info(f"Gettings products from {url}...")
        # current page products
        soup = await self.get_soup_from_url(url)
        if soup is None:
            return

        for product_card_soup in soup.select(self.PRODUCT_CARD_SELECTOR):
            try:
                yield self.get_product_summary(product_card_soup)
            except ProductParseException as e:
                print(e)

        # next page products
        next_page_url = await self.get_next_page_url(soup)
        if next_page_url:
            async for product in self.get_products_from_page(next_page_url):
                yield product

    async def get_next_page_url(self, soup: BeautifulSoup) -> str:
        """
        Returns absolute url for next catalogue page.
        soup - BeautifulSoup object for current page
        """
        raise NotImplementedError

    def get_product_summary(self, product_card) -> ProductSummary:
        raise NotImplementedError

    async def get_soup_from_url(self, url):
        async with self._session.get(
            url, headers={"User-Agent": "Yandex"}
        ) as response:
            if response.status != 200:
                return None
            r_text = await response.text("utf-8")
        soup = BeautifulSoup(r_text, features="lxml")

        return soup

    async def get_product_info(self, product: ProductSummary) -> ProductInfo:
        soup = await self.get_soup_from_url(product.url)
        if soup is None:
            raise ProductParseException("could not fetch product")

        abv = self.get_abv_from_product_info(soup) or product.abv
        bk = self.get_bk_from_product_info(soup) or product.bk
        category = (
            self.get_category_from_product_info(soup) or product.category
        )
        volume = self.get_volume_from_product_info(soup) or product.volume
        brand = self.get_brand_from_product_info(soup) or product.brand

        product_info = ProductInfo(
            **{
                **asdict(product),
                "abv": abv,
                "bk": bk,
                "category": category,
                "volume": volume,
                "brand": brand,
            }
        )

        return product_info

    @staticmethod
    def get_abv_from_product_info(soup):
        return None

    @staticmethod
    def get_bk_from_product_info(soup):
        return None

    @staticmethod
    def get_category_from_product_info(soup):
        return None

    @staticmethod
    def get_volume_from_product_info(soup):
        return None

    @staticmethod
    def get_brand_from_product_info(soup):
        return None

    async def save_product(self, product_info: ProductInfo):
        if brand_title := product_info.brand:
            brand_title = (
                product_info.brand.strip()
                .replace("&apos;", "'")
                .replace("'", "")
                .lower()
            )
            brand, created = await Brand.get_or_create(title=brand_title)
            self.stats["brand"][created] += 1
        else:
            brand = None

        if product_info.category:
            category_title = product_info.category.strip()
            category, created = await Category.get_or_create(
                title=category_title
            )
            self.stats["category"][created] += 1
        else:
            category = None

        product_title = product_info.title.strip()
        product_obj, created = await Product.get_or_create(
            bk=product_info.bk,
            source=product_info.source,
            defaults={
                "brand": brand,
                "category": category,
                "title": product_title,
                "url": product_info.url,
                "image": product_info.image,
                "abv": product_info.abv,
                "volume": product_info.volume,
            },
        )
        self.stats["product"][created] += 1

        await Price.create(
            product=product_obj,
            value=product_info.price,
            in_stock=product_info.in_stock,
        )