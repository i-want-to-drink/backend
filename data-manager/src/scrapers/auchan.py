import json

from scrapers.alco_scraper import AlcoScraper
from enums import EStore
from scrapers.utils import ProductSummary, ProductInfo, ProductParseException


class AuchanAlcoScraper(AlcoScraper):
    BASE_URL = "https://www.auchan.ru"
    START_PATH = "/catalog/alkogol/krepkiy-alkogol/"

    PRODUCT_CARD_SELECTOR = "article.productCard"

    SOURCE = EStore.AUCHAN

    CATEGORY_PATHS = [
        "/catalog/alkogol/vino/",
        "/catalog/alkogol/shampanskoe-i-igristye-vina/",
        "/catalog/alkogol/pivo/",
        "/catalog/alkogol/bezalkogolnye-napitki/",
    ]

    async def get_next_page_url(self, soup) -> str:
        try:
            self.category_iterator
        except AttributeError:
            self.category_iterator = iter(self.CATEGORY_PATHS)

        next_link = soup.select_one("li.pagination-arrow--right > a")
        if next_link:
            return self.BASE_URL + next_link["href"]

        try:
            return self.BASE_URL + next(self.category_iterator)
        except StopIteration:
            pass

    def get_product_summary(self, product_card) -> ProductSummary:
        data_s = product_card.find("script", type="application/ld+json").string
        try:
            data = json.loads(data_s)
        except (TypeError, ValueError):
            raise ProductParseException(f"Failed to parse {data_s} as json")

        try:
            product_title = data["name"]
        except KeyError:
            raise ProductParseException(f"Failed to find key 'name' in {data}")

        try:
            product_image = self.BASE_URL + data["image"]
        except KeyError:
            raise ProductParseException(
                f"Failed to find key 'image' in {data}"
            )

        try:
            product_brand = data["brand"]
        except KeyError:
            product_brand = None

        try:
            product_price = float(data["offers"]["price"])
        except KeyError:
            raise ProductParseException(f"Failed to find price in {data}")
        except ValueError:
            raise ProductParseException(
                f'Failed to convert {data["offers"]["price"]} to float'
            )

        try:
            product_in_stock = data["offers"]["availability"] == "InStock"
        except KeyError:
            raise ProductParseException(f"Failed to find in_stock in {data}")

        product_path = product_card.a.get("href")
        product_url = self.BASE_URL + product_path
        product_summary = ProductSummary(
            title=product_title,
            image=product_image,
            path=product_path,
            url=product_url,
            price=product_price,
            in_stock=product_in_stock,
            source=self.SOURCE,
            brand=product_brand,
        )
        return product_summary

    @staticmethod
    def get_abv_from_product_info(soup):
        try:
            abv = soup.find(
                "th", string="Содержание алкоголя, %"
            ).parent.td.text
        except Exception:
            return

        try:
            # Иногда крепкость записана в странном формате вида "40, 40"
            # пример: https://www.auchan.ru/product/nastfruko_shulc_abs_70_07l
            # "40".split(",") -> ["40"]
            # "40, 40".split(",") -> ["40", "40"]
            abv = abv.split(",")[0]
            return float(abv)
        except (ValueError, IndexError):
            return

    @staticmethod
    def get_bk_from_product_info(soup):
        try:
            return soup.find("th", string="Артикул товара").parent.td.text
        except Exception:
            raise ProductParseException("bk not found")

    @staticmethod
    def get_category_from_product_info(soup):
        return soup.select("nav[aria-label='Вы находитесь здесь:'] a")[-1].text

    @staticmethod
    def get_volume_from_product_info(soup):
        try:
            volume = soup.find("th", string="Объем, л").parent.td.text
            return float(volume) * 1_000
        except Exception:
            pass
