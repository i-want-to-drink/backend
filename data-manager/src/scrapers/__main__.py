import asyncio
import logging

from tortoise import Tortoise

from .auchan import AuchanAlcoScraper
from .lenta import LentaAlcoScraper
from .perekrestok import PerekrestokAlcoScraper
from .av import AvAlcoScraper
from .krasnoeibeloe import KrasnoeIBeloeAlcoScraper

from config import setup


scrapers = [
    AuchanAlcoScraper,
    LentaAlcoScraper,
    PerekrestokAlcoScraper,
    AvAlcoScraper,
    KrasnoeIBeloeAlcoScraper,
]


async def main():
    await setup()

    for Scraper in scrapers:
        async with Scraper() as as_:
            await as_.do_scraping()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())