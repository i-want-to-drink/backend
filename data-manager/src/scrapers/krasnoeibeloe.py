from typing import Set, AsyncIterator
from unicodedata import category

from enums import EStore
from scrapers.alco_scraper import AlcoScraper
from scrapers.utils import ProductInfo


class KrasnoeIBeloeAlcoScraper(AlcoScraper):
    BASE_URL = "https://krasnoeibeloe.ru"
    START_PATH = "/catalog/vodka_nastoyki/"

    SOURCE = EStore.KRASNOEIBELOE

    async def get_products(self):
        categories = await self.mobile_api(
            "https://retail-kb.itnap.ru/api/v1/products/categories"
        )

        for category in categories["categories"]:
            if category["name"] not in [
                "Водка, настойки",
                "Виски, бурбон",
                "Коньяк, арманьяк",
                "Текила, ром, ликер",
                "Вино импорт",
                "Вино Россия",
                "Вино с оценкой",
                "Вино игристое, вермут",
                "Коктейли",
            ]:
                continue

            category_products = await self.mobile_api(
                "https://retail-kb.itnap.ru/api/v1/products",
                params={"category_id": category["category_id"]},
            )
            for product in category_products["products"]:
                yield ProductInfo(
                    title=product["name"],
                    image=product["img"],
                    path="",
                    url="",
                    price=product["price"],
                    source=self.SOURCE,
                    in_stock=product["quantity"] > 0,
                    bk=product["product_id"],
                    category=category["name"],
                    brand=None,
                    abv=product["degree"],
                    volume=product["measure"].rstrip(" л"),
                )

    async def mobile_api(self, url, params=None):
        HEADERS = {
            "Host": "retail-kb.itnap.ru",
            "Accept": "*/*",
            "Accept-Language": "ru",
            "Connection": "keep-alive",
            "Accept-Encoding": "gzip, deflate, br",
            "User-Agent": "RedWhite/2.18.3 (iPhone 7; iOS 14.0.1; 1; 3494A92F-1B3A-4A2C-80D5-71B9F88B1BCC)",
        }
        async with self._session.get(
            url, params=params, headers=HEADERS
        ) as response:
            r_json = await response.json()
        return r_json["result"]
