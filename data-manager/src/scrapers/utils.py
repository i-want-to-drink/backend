from dataclasses import dataclass
from typing import Optional

from enums import EStore

# TYPING
@dataclass
class ProductInfo:
    title: str
    image: str
    path: str
    url: str
    price: float
    source: EStore
    in_stock: bool

    bk: str
    category: Optional[str] = None
    brand: Optional[str] = None
    abv: Optional[float] = None
    volume: Optional[float] = None


@dataclass
class ProductSummary(ProductInfo):
    brand: Optional[str] = None
    bk: Optional[str] = None
    category: Optional[str] = None
    brand: Optional[str] = None
    abv: Optional[float] = None
    volume: Optional[float] = None


# EXCEPTIONS


class ProductParseException(Exception):
    pass