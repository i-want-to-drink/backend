from enums import EStore
from scrapers.alco_scraper import AlcoScraper
from scrapers.utils import ProductSummary, ProductInfo, ProductParseException


class PerekrestokAlcoScraper(AlcoScraper):
    BASE_URL = "https://www.vprok.ru"
    START_PATH = "/catalog/1997/alkogol"

    PRODUCT_CARD_SELECTOR = ".xf-catalog__item"
    NEXT_PAGE_LINK_SELECTOR = "link[rel=next]"

    SOURCE = EStore.PEREKRESTOK

    async def get_next_page_url(self, soup) -> str:
        next_link = soup.select_one("link[rel=next]")
        if next_link:
            return self.BASE_URL + next_link["href"]

    def get_product_summary(self, product_card) -> ProductSummary:
        product_category = product_card.div["data-owox-category-name"]
        product_title = product_card.div["data-owox-product-name"]
        product_image = product_card.select_one("img")["src"]
        product_path = product_card.div["data-product-card-url"]
        product_url = self.BASE_URL + product_path
        product_price = product_card.div["data-owox-product-price"]
        product_in_stock = product_card.div["data-owox-is-available"]
        product_brand = product_card.div["data-owox-product-vendor-name"]
        product_bk = product_card.div["data-owox-product-id"]
        product_category = product_card.div["data-owox-category-name"]

        product_summary = ProductSummary(
            title=product_title,
            image=product_image,
            path=product_path,
            url=product_url,
            price=product_price,
            in_stock=product_in_stock,
            source=self.SOURCE,
            brand=product_brand,
            bk=product_bk,
            category=product_category,
        )
        return product_summary

    @staticmethod
    def get_volume_from_product_info(soup):
        for section_property in soup.select(
            ".xf-product-new-about-section__property"
        ):
            property_name = section_property.select_one(
                ".xf-product-new-about-section__property__name"
            ).text.strip()
            if property_name != "Объем":
                continue

            property_value = section_property.select_one(
                ".xf-product-new-about-section__property__value-content"
            ).text.strip()

            try:
                return float(property_value.strip().rstrip("л")) * 1_000
            except Exception:
                pass

    @staticmethod
    def get_abv_from_product_info(soup):
        for section_property in soup.select(
            ".xf-product-new-about-section__property"
        ):
            property_name = section_property.select_one(
                ".xf-product-new-about-section__property__name"
            ).text.strip()
            if property_name != "Крепость, %":
                continue

            property_value = section_property.select_one(
                ".xf-product-new-about-section__property__value-content"
            ).text.strip()

            try:
                return float(property_value.strip())
            except Exception:
                pass


async def main():
    async with PerekrestokAlcoScraper() as pas:
        await pas.do_scraping()
