import re

from scrapers.alco_scraper import AlcoScraper
from enums import EStore
from scrapers.utils import ProductSummary, ProductInfo, ProductParseException


class AvAlcoScraper(AlcoScraper):
    BASE_URL = "https://av.ru"
    START_PATH = "/catalog/alkogol/krepkie-napitki/"

    PRODUCT_CARD_SELECTOR = "div.b-product"

    SOURCE = EStore.AV

    CATEGORY_PATHS = [
        "/catalog/alkogol/vino/",
        "/catalog/alkogol/pivo-sidr-kokteyli/",
        "/catalog/alkogol/shampanskoe-i-igristye-vina/",
    ]

    async def get_next_page_url(self, soup) -> str:
        try:
            self.category_iterator
        except AttributeError:
            self.category_iterator = iter(self.CATEGORY_PATHS)

        next_link = soup.select_one("a.b-pager__btn_next")
        if next_link:
            return self.BASE_URL + next_link["href"]

        try:
            return self.BASE_URL + next(self.category_iterator)
        except StopIteration:
            pass

    def get_product_summary(self, product_card) -> ProductSummary:
        product_title = product_card["data-name"]
        product_image = product_card.a.img["data-lazy-src"]
        product_path = product_card.a["href"]
        product_url = self.BASE_URL + product_path
        product_price = product_card["data-unit-price"]
        product_in_stock = True
        product_brand = product_card["data-brand"]
        product_bk = product_card["data-code"]
        product_category = product_card["data-category-name"].lstrip(
            "Крепкие напитки/"
        )

        product_volume = None
        try:
            # example: / 1 шт (700 г)
            measure = product_card["data-measure"]
            volume_search = re.findall(r"\(([\d\s]+)", measure)
            if volume_search:
                product_volume = int(volume_search[0].replace(" ", ""))
        except Exception as e:
            pass

        product_summary = ProductSummary(
            title=product_title,
            image=product_image,
            path=product_path,
            url=product_url,
            price=product_price,
            in_stock=product_in_stock,
            source=self.SOURCE,
            brand=product_brand,
            category=product_category,
            bk=product_bk,
            volume=product_volume,
        )
        return product_summary

    @staticmethod
    def get_abv_from_product_info(soup):
        for section_property in soup.select(".b-goods-info__row"):
            property_name = section_property.select_one(
                ".b-goods-info__name"
            ).text.strip()

            if property_name != "Содержание спирта:":
                continue

            property_value = section_property.select_one(
                ".b-goods-info__value"
            ).text.strip()

            try:
                return float(property_value.strip())
            except Exception:
                pass

    @staticmethod
    def get_volume_from_product_info(soup):
        # example: / 1 шт (700 г)
        measure = soup.select_one(".b-goods-price__measure").text

        volume_search = re.match(r"\(([\d\s]+)", measure)
        if volume_search:
            return int(volume_search.group(1).replace(" ", ""))