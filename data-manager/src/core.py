from typing import List

from tortoise.query_utils import Q

from models import Brand, Category, Product


def get_qs(q):
    return Product.filter(Q(title__icontains=q) | Q(brand__title__icontains=q))


async def search(q: str, offset: int, limit: int) -> List[int]:
    """
    Returns list of product ids.
    """
    results = get_qs(q)
    if offset > 0:
        results = results.offset(offset)
    if limit > 0:
        results = results.limit(limit)

    # TODO: sort results
    async for r in results.values_list("id", flat=True):
        yield r


async def count(q: str) -> int:
    """
    Returns count of products.
    """
    results = get_qs(q)
    results_count = await results.count()

    return results_count
